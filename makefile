all: pull compile

pull:
	git submodule foreach git checkout master
	git submodule foreach git pull origin master

compile:
	xelatex ./userguide.tex
	xelatex ./technicalspec.tex
# And again so contents are build correctly
	xelatex ./userguide.tex 1> /dev/null
	xelatex ./technicalspec.tex 1> /dev/null
